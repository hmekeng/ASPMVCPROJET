﻿CREATE TABLE [dbo].[media](
	[idMedia] [int] IDENTITY(1,1) NOT NULL,
	[nomMedia] [varchar](50) NULL,
	[mediaPath] [varchar](50) NOT NULL,
	[createTime] [datetime] NULL,
	[updateTime] [datetime] NULL,
	[title] [nvarchar](50) NULL,
	[shortDescription] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[idMedia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]