﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppTravellers.DAL
{
    public class Comments
    {

        public int IdComment { get; set; }
        [Required]
        [Display(Name = "Content")]
        public string CommentContent { get; set; }
        [DefaultValue(0)]
        public int LikeCount { get; set; }
        [DefaultValue(false)]
        public bool Deleted { get; set; }
        [Required]
        [Display(Name = "Reply")]
        public int ReplyComment { get; set; }
        public DateTime CreateTime { get; set; }
        public Articles Articles { get; set; }
        public UserAccount UserAccount { get; set; }

        public Comments()
        {
        }

        public Comments(string commentContent, int likeCount, bool deleted, int replyComment, DateTime createTime, Articles articles, UserAccount userAccount)
        {
            CommentContent = commentContent;
            LikeCount = likeCount;
            Deleted = deleted;
            ReplyComment = replyComment;
            CreateTime = createTime;
            Articles = articles;
            UserAccount = userAccount;
        }

        public Comments(int idComment, string commentContent, int likeCount, bool deleted, int replyComment, DateTime createTime, Articles articles, UserAccount userAccount)
        {
            IdComment = idComment;
            CommentContent = commentContent;
            LikeCount = likeCount;
            Deleted = deleted;
            ReplyComment = replyComment;
            CreateTime = createTime;
            Articles = articles;
            UserAccount = userAccount;
        }
    }

}
