﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppTravellers.WEB.Models
{
    public class LoginModel
    {

        [Display(Name = "Your UserName")]
        [StringLength(30, ErrorMessage = ":Less than 30 characters")]
        public string Login { get; set; }
        [Required]
        [StringLength(20)]
        public string Password { get; set; }
    }
}