﻿CREATE TABLE [dbo].[userAccount](
	[idUserAccount] [int] IDENTITY(1,1) NOT NULL,
	[firstName] [varchar](100) NOT NULL,
	[lastName] [varchar](100) NOT NULL,
	[password] [varchar](50) NOT NULL,
	[login] [varchar](50) NOT NULL,
	[createTime] [smalldatetime] NOT NULL,
	[updateTime] [smalldatetime] NULL,
	[isAdmin] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[idUserAccount] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


GO
ALTER TABLE [dbo].[userAccount] ADD  DEFAULT ((0)) FOR [isAdmin]