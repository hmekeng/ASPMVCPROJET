﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppTravellers.DAL
{
    public class Villes
    {
        public int IdVille { get; set; }
        [Required]
        [Display(Name = "Where where you?")]
        public string NomVille { get; set; }
        [Required]
        [Display(Name = "ShortDescription")]
        public string Descriptif { get; set; }
        public Pays Pays { get; set; } // ici nous avons la foreign key pays et puisque on a besoin on implemente un objet de type pays avec ses prieté



        //contructeur vide 

        public Villes()
        {
        }

        public Villes(string nomVille, string descriptif, Pays pays)
        {
            NomVille = nomVille;
            Descriptif = descriptif;
            Pays = Pays;
        }

        public Villes(int idVille, string nomVille, string descriptif, Pays pays)
        {
            NomVille = nomVille;
            Descriptif = descriptif;
            Pays = Pays;
            IdVille = idVille;
        }


    }
}
