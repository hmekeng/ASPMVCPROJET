﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppTravellers.DAL
{
    public class UserAccount
    {
        public int IdUserAccount { get; set; }
        
        [Required]
        [StringLength(50, ErrorMessage = ":Less than 30 characters")]
        [Display(Name = "FirstName")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "LastName")]
        public string LastName { get; set; }
        [Required]
        [Display(Name = "Your Password")]

        public string Password{ get; set; }
        [Required]

        [Display(Name = "Your UserName")]
        [StringLength(30, ErrorMessage = ":Less than 30 characters")]
        public string Login  { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; } // the date we modified our Account it simple note a DateTime for the moment we create

        public bool? IsAdmin { get; set; } // is not a admin at the first connection

        public UserAccount() // constructeur vide car chaque user doit creer son compte
        {

        }

        public UserAccount(string firstName, string lastName, string password, string login, DateTime createTime, DateTime? updateTime, bool? isAdmin)
        {
            FirstName = firstName;
            LastName = lastName;
            Password = password;
            Login = login;
            CreateTime = createTime;
            UpdateTime = updateTime;
            IsAdmin = isAdmin;
        }

        public UserAccount(int idUserAccount, string firstName, string lastName, string password, string login, DateTime createTime, DateTime? updateTime, bool? isAdmin)
        {
            IdUserAccount = idUserAccount;
            FirstName = firstName;
            LastName = lastName;
            Password = password;
            Login = login;
            CreateTime = createTime;
            UpdateTime = updateTime;
            IsAdmin = isAdmin;
        }

    }
}
