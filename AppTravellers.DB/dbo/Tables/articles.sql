﻿CREATE TABLE [dbo].[articles](
	[idArticle] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](max) NOT NULL,
	[articleContent] [nvarchar](max) NOT NULL,
	[createTime] [datetime] NOT NULL,
	[updateTime] [datetime] NULL,
	[fk_idUserAccount] [int] NOT NULL,
	[fk_idVille] [int] NOT NULL,
	[published] [bit] NOT NULL,
	[fk_idImage] [int] NOT NULL,
	[shortDescription] [nvarchar](50) NULL,
	[metaDescription] [nvarchar](max) NULL,
	[likeCount] [int] NULL,
    [fk_idMedia] INT  NOT NULL, 
    PRIMARY KEY CLUSTERED 
(
	[idArticle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


GO
ALTER TABLE [dbo].[articles]  WITH CHECK ADD FOREIGN KEY([fk_idImage])
REFERENCES [dbo].[images] ([idImage])
GO
ALTER TABLE [dbo].[articles]  WITH CHECK ADD FOREIGN KEY([fk_idUserAccount])
REFERENCES [dbo].[userAccount] ([idUserAccount])
GO
ALTER TABLE [dbo].[articles]  WITH CHECK ADD FOREIGN KEY([fk_idVille])
REFERENCES [dbo].[villes] ([idVille])
GO
ALTER TABLE [dbo].[articles]  WITH CHECK ADD FOREIGN KEY([fk_idMedia])
REFERENCES [dbo].[media] ([idMedia])