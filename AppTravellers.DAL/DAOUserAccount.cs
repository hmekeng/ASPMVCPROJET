﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppTravellers.DAL
{
     public class DAOUserAccount : DAOCon, IDAO<UserAccount>
    {
        public UserAccount create(UserAccount objet)
        {

            objet.CreateTime = DateTime.Now;
            //on prépare notre requête Et il faut qu'on puisse récupérer l'id auto-généré par la DB
           SqlCommand command = connection.CreateCommand();
            //OUTPUT inserted.idUserAccount on recupere ici l'idUserAccount car au moment de la creation on ne connait pas l id 
            string query = "INSERT INTO UserAccount (firstName, lastName, password, login, createTime, isAdmin) OUTPUT inserted.idUserAccount VALUES (@firstName, @lastName, @password, @login, @createTime,  @isAdmin)";
            command.Parameters.AddWithValue("@firstName", objet.FirstName);
            command.Parameters.AddWithValue("@lastName", objet.LastName);
            command.Parameters.AddWithValue("@password", objet.Password);
            command.Parameters.AddWithValue("@login", objet.Login);
            command.Parameters.AddWithValue("@createTime", objet.CreateTime);
            if(objet.IsAdmin==null)command.Parameters.AddWithValue("@isAdmin", DBNull.Value);
            else command.Parameters.AddWithValue("@isAdmin", objet.IsAdmin);
            command.CommandText = query;
            //On ouvre la connection juste avant l'execution de la commande
            connection.Open();
            //ExecuteScalar exécute une requête et renvoye un OBJET
            objet.IdUserAccount=(int)command.ExecuteScalar();
            return (objet);
        }



        public bool IsUserExist(string login, string password)
        {
            //Voir si quelqu'un a ce login et ce mdp dans notre database de données
            //si c'est non, on retourne directement null
            bool Exist = false;
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT idUserAccount FROM userAccount WHERE login = @login AND password = @password";
            command.Parameters.AddWithValue("@login", login);
            command.Parameters.AddWithValue("@password", password);
            connection.Open();
            command.CommandText = query;
           int IdUserAccount = (int)command.ExecuteScalar();
            if (IdUserAccount !=0) Exist = true;

            connection.Close();
            return Exist;
          

        }


        public  bool UserIsValid(string login, string password)
        {


            bool authenticated = false;
            string query = string.Format("SELECT * FROM [user] WHERE Username= '{0}' AND Password= '{1}' ", login, password);

            SqlCommand cmd = new SqlCommand(query, connection);
            connection.Open();
            SqlDataReader sdr = cmd.ExecuteReader();
            authenticated = sdr.HasRows;
            connection.Close();

            return (authenticated);
        }



        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public UserAccount read(int id)
        {
            throw new NotImplementedException();
        }

        public List<UserAccount> readAll()
        {
            throw new NotImplementedException();
        }

        public UserAccount update(UserAccount objet)
        {
            throw new NotImplementedException();
        }
    }
}
