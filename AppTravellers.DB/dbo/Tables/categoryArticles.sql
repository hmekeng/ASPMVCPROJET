﻿CREATE TABLE [dbo].[categoryArticles](
	[idCategoryArticle] [int] IDENTITY(1,1) NOT NULL,
	[fk_idArticle] [int] NOT NULL,
	[fk_idCategory] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idCategoryArticle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


GO
ALTER TABLE [dbo].[categoryArticles]  WITH CHECK ADD FOREIGN KEY([fk_idArticle])
REFERENCES [dbo].[articles] ([idArticle])
GO
ALTER TABLE [dbo].[categoryArticles]  WITH CHECK ADD FOREIGN KEY([fk_idCategory])
REFERENCES [dbo].[categories] ([idCategory])