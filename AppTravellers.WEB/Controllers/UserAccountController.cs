﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AppTravellers.DAL;
using AppTravellers.WEB.Models;
using System.Web.Security;

namespace AppTravellers.WEB.Controllers
{
    public class UserAccountController : Controller
    {
        private DAOUserAccount daoUser = new DAOUserAccount();
        // GET: UserAccount
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult CreateAccount()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateAccount(UserModel Account)
        {
            UserAccount userAccount = new UserAccount();
            userAccount.Login = Account.Login;
            userAccount.Password = Account.Password;
            userAccount.LastName = Account.LastName;
            userAccount.FirstName = Account.FirstName;


            userAccount = daoUser.create(userAccount);

           

           return RedirectToAction("Index",  "home");
        }

        //[HttpGet]
        //public ActionResult Login()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult login(UserModel userAccount)
        //{
        //    string message = "";

        //    if (ModelState.IsValid)
        //    {
        //        if (daoUser.IsUserExist(userAccount.Login, userAccount.Password))
        //        {
        //            ;
        //        }
        //        else
        //        {
        //            ModelState.AddModelError("", "login or Password Incorrect.");
        //        }
        //    }
        //    return View();
        //}

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                //if login and password exist and store 
                if (daoUser.UserIsValid(model.Login, model.Password))
                {
                    FormsAuthentication.SetAuthCookie(model.Login, false);
                    //return to the index view of home controller
                    return RedirectToAction("Index", "home");
                }

                {
                    ModelState.AddModelError("", "Invalid login or password");
                }

            }
            return View();//here if not valid the usual name will stay to the same view
        }


        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SubmitRegisterDetails(LoginModel register)
        {
            return View(register);
        }


    }







   
}