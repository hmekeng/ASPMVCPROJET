﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppTravellers.DAL
{
    //Cette interface nous permet d'avoir un modèle pour les actions qu'on veut effectuer dans notre DAL.
    //T correspond aux DAO qu'on veut travailler
    interface IDAO<T>
    {

            //Exemple de 5 méthodes générales, les méthodes particulières, elles, se retrouveront dans la classe

            //Crée un objet de ce type là
            T create(T objet);
            //renvoie un id spécifique
            T read(int id);
            //Revoie une liste de tous les enregistrements
            List<T> readAll();
            //mets à jour
            T update(T objet);
            //efface une entrée par rapport à son id
            void delete(int id);
        }
    
}
