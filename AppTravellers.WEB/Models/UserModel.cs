﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppTravellers.WEB.Models
{
    public class UserModel
    {
        [Required]
        [StringLength(50, ErrorMessage = ":Less than 30 characters")]
        [Display(Name = "FirstName")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "LastName")]
        public string LastName { get; set; }
        [Required]
        [Display(Name = "Your Password")]

        public string Password { get; set; }
        [Required]

        [Display(Name = "Your UserName")]
        [StringLength(30, ErrorMessage = ":Less than 30 characters")]
        public string Login { get; set; }
    }
}