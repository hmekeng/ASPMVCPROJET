﻿CREATE TABLE [dbo].[comments](
	[idComment] [int] IDENTITY(1,1) NOT NULL,
	[commentContent] [nvarchar](max) NOT NULL,
	[createTime] [datetime] NOT NULL,
	[fk_idUserAccount] [int] NOT NULL,
	[fk_idArticle] [int] NOT NULL,
	[likeCount] [int] NULL,
	[deleled] [bit] NULL,
	[replyComment] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[idComment] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


GO
ALTER TABLE [dbo].[comments]  WITH CHECK ADD FOREIGN KEY([fk_idArticle])
REFERENCES [dbo].[articles] ([idArticle])
GO
ALTER TABLE [dbo].[comments]  WITH CHECK ADD FOREIGN KEY([fk_idUserAccount])
REFERENCES [dbo].[userAccount] ([idUserAccount])