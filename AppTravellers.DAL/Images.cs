﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppTravellers.DAL
{
    public class Images
    {
        
        public int  IdImages { get; set; }
        [Required]
        [Display(Name = "ImagePath")]
        public string ImagePath { get; set; }
        public int Size { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }
        [Required]
        [Display(Name = "ShortDescription")]
        public string ShortDescription { get; set; }

        public Images()
        {
        }

        public Images(string imagePath, int size, DateTime createTime, DateTime? updateTime, string title, string shortDescription)
        {
            ImagePath = imagePath;
            Size = size;
            CreateTime = createTime;
            UpdateTime = updateTime;
            Title = title;
            ShortDescription = shortDescription;
        }

        public Images(int idImages, string imagePath, int size, DateTime createTime, DateTime? updateTime, string title, string shortDescription)
        {
            IdImages = idImages;
            ImagePath = imagePath;
            Size = size;
            CreateTime = createTime;
            UpdateTime = updateTime;
            Title = title;
            ShortDescription = shortDescription;
        }
    }
}
