﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppTravellers.DAL
{
    public class Articles
    {
        public int IdArticle { get; set; }
        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }
        [Required]
        [Display(Name = "ShortDescription")]
        public string ShortDescription { get; set; }
        [Required]
        [Display(Name = "My Travel Stories")]
        public string ArticleContent { get; set; }
    
        [Required]
        [Display(Name = "MetaDescription")]
        public string Meta { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool Published { get; set; }
        [DefaultValue(0)]
        public int LikeCount { get; set; }
        public Villes Villes { get; set; }
        public Images Images { get; set; }
        public Media Media { get; set; }
        public UserAccount UserAccount { get; set; }

        public Articles()
        {
        }

        public Articles(string title, string shortDescription, string articleContent, string meta, DateTime createTime, DateTime? updateTime, bool published, int likeCount, Villes villes, Images images, Media media, UserAccount userAccount)
        {
            Title = title;
            ShortDescription = shortDescription;
            ArticleContent = articleContent;
            Meta = meta;
            CreateTime = createTime;
            UpdateTime = updateTime;
            Published = published;
            LikeCount = likeCount;
            Villes = villes;
            Images = images;
            Media = media;
            UserAccount = userAccount;
        }

        public Articles(int idArticle, string title, string shortDescription, string articleContent, string meta, DateTime createTime, DateTime? updateTime, bool published, int likeCount, Villes villes, Images images, Media media, UserAccount userAccount)
        {
            IdArticle = idArticle;
            Title = title;
            ShortDescription = shortDescription;
            ArticleContent = articleContent;
            Meta = meta;
            CreateTime = createTime;
            UpdateTime = updateTime;
            Published = published;
            LikeCount = likeCount;
            Villes = villes;
            Images = images;
            Media = media;
            UserAccount = userAccount;
        }
    }
}
