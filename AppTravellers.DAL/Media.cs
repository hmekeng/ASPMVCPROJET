﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppTravellers.DAL
{
      public class Media
    {
        public int IdMedia { get; set; }
        [Required]
        [Display(Name = "MediaPath")]
        public string MediaPath { get; set; }
        [Required]
        [Display(Name = "Media")]
        public string NomMedia { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }
        [Required]
        [Display(Name = "ShortDescription")]
        public string ShortDescription { get; set; }

        public Media()
        {
        }

        public Media(string mediaPath, string nomMedia, DateTime createTime, DateTime? updateTime, string title, string shortDescription)
        {
            MediaPath = mediaPath;
            NomMedia = nomMedia;
            CreateTime = createTime;
            UpdateTime = updateTime;
            Title = title;
            ShortDescription = shortDescription;
        }

        public Media(int idMedia, string mediaPath, string nomMedia, DateTime createTime, DateTime? updateTime, string title, string shortDescription)
        {
            IdMedia = idMedia;
            MediaPath = mediaPath;
            NomMedia = nomMedia;
            CreateTime = createTime;
            UpdateTime = updateTime;
            Title = title;
            ShortDescription = shortDescription;
        }
    }
}
