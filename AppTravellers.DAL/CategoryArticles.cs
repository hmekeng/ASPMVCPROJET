﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppTravellers.DAL
{
   public class CategoryArticles
    {
        public int IdCategory { get; set; }
        public Articles Articles{ get; set; }
        public Categories Categories { get; set; }

        public CategoryArticles()
        {
        }

        public CategoryArticles(Articles articles, Categories categories)
        {
            Articles = articles;
            Categories = categories;
        }

        public CategoryArticles(int idCategory, Articles articles, Categories categories)
        {
            IdCategory = idCategory;
            Articles = articles;
            Categories = categories;
        }
    }
}
