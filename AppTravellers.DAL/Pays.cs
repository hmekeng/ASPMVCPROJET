﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppTravellers.DAL
{
    public class Pays
    {
        public int IdPays { get; set; }
        [Required]
        [Display(Name = "Country")]
        public string NomPays { get; set; }
        [Required]
        [Display(Name = "ShortDescription")]
        public string Descriptif{ get; set; }

        public Pays()
        {
        }

        public Pays(string nomPays, string descriptif)
        {
            NomPays = nomPays;
            Descriptif = descriptif;
            
        }

        public Pays(int idPays, string nomPays, string descriptif)
        {
            NomPays = nomPays;
            Descriptif = descriptif;
            IdPays = idPays;
        }


    }
}
