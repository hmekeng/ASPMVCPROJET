﻿CREATE TABLE [dbo].[images](
	[idImage] [int] IDENTITY(1,1) NOT NULL,
	[imagePath] [varchar](50) NOT NULL,
	[size] [int] NULL,
	[createTime] [datetime] NOT NULL,
	[updateTime] [datetime] NOT NULL,
	[title] [nvarchar](50) NULL,
	[shortDescription] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[idImage] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

