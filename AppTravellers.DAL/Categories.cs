﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppTravellers.DAL
{
    public class Categories
    {
        public string IdCategory { get; set; }
        [Required]
        [Display(Name = "Category")]
        public string NomCategory { get; set; }

        public Categories()
        {
        }

        public Categories(string nomCategory)
        {
            NomCategory = nomCategory;
        }

        public Categories(string idCategory, string nomCategory) : this(idCategory)
        {
            NomCategory = nomCategory;
        }
    }
}
